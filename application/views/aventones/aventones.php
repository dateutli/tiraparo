<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Logo Nav - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>css/logo-nav.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- bootstrap slider -->
    <link href="<?php echo base_url();?>css/slider.css" rel="stylesheet">

    <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="http://placehold.it/150x50&text=Logo" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#">Aventones</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
              <ul class="nav nav-pills">
                <li class="active">
                  <a data-toggle="tab" href="#home">
                    Mapa <i class="fa fa-map" aria-hidden="true"></i>
                  </a>
                </li>
                <li>
                  <a data-toggle="tab" href="#menu1">
                    Lista <i class="fa fa-list" aria-hidden="true"></i>
                  </a>
                </li>
                <li>
                  <a data-toggle="tab" href="#menu2">
                    Mis Aventones <i class="fa fa-car" aria-hidden="true"></i>
                  </a>
                </li>
              </ul>

              <div class="tab-content">
                <!-- ************** MAPA ************** -->
                <div id="home" class="tab-pane fade in active">
                  <hr />
                  <div class="col-lg-8">
                    <div id="map"></div>
                  </div>
                  <div class="col-lg-4">
                    <!-- Trip Info Panel -->
                    <div class="panel panel-default" id="tripPanel" hidden>
                      <div class="panle-heading pull-right">
                        <button class="btn btn-default" id="tripPanel-close">
                          <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                      </div>
                      <div class="panel-body">
                        <!-- Imagen -->
                        <img src="<?php echo base_url();?>img/default-user-image.png" class="img-responsive img-circle center-block" style="height:100px;"/>
                      </div>
                      <ul class="list-group">
                        <!-- Informacion del Conductor -->
                        <li class="list-group-item">
                          <div id="panel-conductorNombre"></div>
                        </li>
                        <li class="list-group-item">
                          <div id="panel-conductorRating"></div>
                        </li>
                        <!-- Origen -->
                        <li class="list-group-item">
                          <div id="panel-origenDireccion"></div>
                        </li>
                        <!-- Destino -->
                        <li class="list-group-item">
                          <div id="panel-destinoDireccion"></div>
                        </li>
                        <!-- Fecha -->
                        <li class="list-group-item">
                          <div id="panel-fecha"></div>
                        </li>
                        <!-- Espacios -->
                        <li class="list-group-item">
                          <div id="panel-pasajeros"></div>
                        </li>
                        <li class="list-group-item">
                          <button class="btn btn-default btn-block" id="quieroRide">
                            Quiero Ride <i class="fa fa-plus-circle" aria-hidden="true"></i>
                          </button>
                        </li>
                      </ul>
                    </div>
                    <!-- Filtros Panel -->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">Filtros <i class="fa fa-sliders" aria-hidden="true"></i></h3>
                      </div>
                      <div class="panel-body">
                        <label>
                          Dia
                        </label>
                        <select class="form-control" id="filtro-dia">
                          <option>
                            2017-12-01
                          </option>
                          <option>
                            2017-12-02
                          </option>
                          <option>
                            2017-12-03
                          </option>
                          <option>
                            2017-12-04
                          </option>
                          <option>
                            2017-12-05
                          </option>
                          <option>
                            2017-12-06
                          </option>
                          <option>
                            2017-12-07
                          </option>
                        </select>
                        <label>
                          Horario
                        </label>
                        <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="287" data-slider-step="1" data-slider-value="[72, 252]" data-slider-id="hours_slider" id="hours_slider" data-slider-tooltip="hide" />
                        <div class="col-xs-6">
                          <input class="form-control text-center" id="hora1" disabled>
                        </div>
                        <div class="col-xs-6">
                          <input class="form-control text-center" id="hora2" disabled>
                        </div>
                        <!-- <label>
                          Destino
                        </label>
                        <select class="form-control" id="filtro-destino">
                          <option>
                            Cualquiera
                          </option>
                          <option>
                            UACJ Campus IIT-IADA
                          </option>
                          <option>
                            UACJ Campus ICB
                          </option>
                        </select> -->
                        <label>
                        </label>
                        <button class="btn btn-primary btn-block" type="button" id="filtro-boton">
                          Filtrar
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- ************* LISTA  ************* -->
                <div id="menu1" class="tab-pane fade">
                  <hr />
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <!-- Imagen -->
                      <div class="col-lg-1">
                        <img src="<?php echo base_url();?>img/default-user-image.png" class="img-responsive img-circle"/>
                      </div>
                      <!-- Origen -->
                      <div class="col-lg-2 text-center">
                        <label>
                          Origen
                        </label>
                        <div>
                          Las Misiones
                        </div>
                      </div>
                      <!-- Destino -->
                      <div class="col-lg-2 text-center">
                        <label>
                          Destino
                        </label>
                        <div>
                          UACJ IADA
                        </div>
                      </div>
                      <!-- Fecha -->
                      <div class="col-lg-2 text-center">
                        <label>
                          Fecha de Partida
                        </label>
                        <div>
                          11/12/17 23:00
                        </div>
                      </div>
                      <!-- Pasajeros -->
                      <div class="col-lg-2 text-center">
                        <label>
                          Espacios
                        </label>
                        <div>
                          <i class="fa fa-users" aria-hidden="true"></i> x 3
                        </div>
                      </div>
                      <!-- Unirse -->
                      <div class="col-lg-2 text-center">
                        <label>

                        </label>
                        <div>
                          <button class="btn btn-default">
                            Quiero Ride <i class="fa fa-plus-circle" aria-hidden="true"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="menu2" class="tab-pane fade">
                  <hr />
                  <div class="col-lg-4 col-lg-offset-4">
                    <form method="POST" action="<?php echo base_url();?>aventones/insertRequest">
                      <!-- Origen -->
                      <label>
                        Origen
                      </label>
                      <input name="crear-origen" class="form-control" id="autocomplete1" placeholder="Escribe tu punto de partida" onFocus="geolocate()" type="text" required></input>
                      <input name="crear-origen-coordenadas" hidden id="autocomplete1-coordenadas"/>
                      <!-- Destino -->
                      <label>
                        Destino
                      </label>
                      <input name="crear-destino" class="form-control" id="autocomplete2" placeholder="Escribe tu destino" onFocus="geolocate()" type="text" required></input>
                      <input name="crear-destino-coordenadas" hidden id="autocomplete2-coordenadas"/>
                      <!-- Fecha -->
                      <label>
                        Fecha de Partida
                      </label>
                      <select class="form-control" name="crear-fecha">
                        <option>
                          2017-12-01
                        </option>
                        <option>
                          2017-12-02
                        </option>
                        <option>
                          2017-12-03
                        </option>
                        <option>
                          2017-12-04
                        </option>
                        <option>
                          2017-12-05
                        </option>
                        <option>
                          2017-12-06
                        </option>
                        <option>
                          2017-12-07
                        </option>
                      </select>
                      <!-- Hora -->
                      <label>
                        Hora de Partida
                      </label>
                      <input type="text" class="span1" value="" data-slider-min="0" data-slider-max="287" data-slider-step="1" data-slider-value="144" data-slider-id="hours_slider2" id="hours_slider2" data-slider-tooltip="hide" />
                      <input name="crear-hora" class="form-control text-center" id="hora3" readonly>
                      <!-- Espacios -->
                      <label>
                        Espacios
                      </label>
                      <input type="number" class="form-control" name="crear-espacios" onFocus="ADR2LATLNG()" required/>

                      <label></label>
                      <button class="btn btn-success btn-block">
                        Crear Viaje
                      </button>
                    </form>
                  </div>

                </div>
              </div>
            </div>
        </div>
    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url();?>js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>

    <!-- Bootstrap slider -->
    <script src="<?php echo base_url();?>js/bootstrap-slider.js"></script>

    <script>
      var directionsService;
      var directionsDisplay;
      var placeSearch, autocomplete1, autocomplete2;
      var map, marker;
      var markers = [];
      var hours = ['0:00', '0:05', '0:10', '0:15', '0:20', '0:25', '0:30', '0:35', '0:40', '0:45', '0:50', '0:55', '1:00', '1:05', '1:10', '1:15', '1:20', '1:25', '1:30', '1:35', '1:40', '1:45', '1:50', '1:55', '2:00', '2:05', '2:10', '2:15', '2:20', '2:25', '2:30', '2:35', '2:40', '2:45', '2:50', '2:55', '3:00', '3:05', '3:10', '3:15', '3:20', '3:25', '3:30', '3:35', '3:40', '3:45', '3:50', '3:55', '4:00', '4:05', '4:10', '4:15', '4:20', '4:25', '4:30', '4:35', '4:40', '4:45', '4:50', '4:55', '5:00', '5:05', '5:10', '5:15', '5:20', '5:25', '5:30', '5:35', '5:40', '5:45', '5:50', '5:55', '6:00', '6:05', '6:10', '6:15', '6:20', '6:25', '6:30', '6:35', '6:40', '6:45', '6:50', '6:55', '7:00', '7:05', '7:10', '7:15', '7:20', '7:25', '7:30', '7:35', '7:40', '7:45', '7:50', '7:55', '8:00', '8:05', '8:10', '8:15', '8:20', '8:25', '8:30', '8:35', '8:40', '8:45', '8:50', '8:55', '9:00', '9:05', '9:10', '9:15', '9:20', '9:25', '9:30', '9:35', '9:40', '9:45', '9:50', '9:55', '10:00', '10:05', '10:10', '10:15', '10:20', '10:25', '10:30', '10:35', '10:40', '10:45', '10:50', '10:55', '11:00', '11:05', '11:10', '11:15', '11:20', '11:25', '11:30', '11:35', '11:40', '11:45', '11:50', '11:55', '12:00', '12:05', '12:10', '12:15', '12:20', '12:25', '12:30', '12:35', '12:40', '12:45', '12:50', '12:55', '13:00', '13:05', '13:10', '13:15', '13:20', '13:25', '13:30', '13:35', '13:40', '13:45', '13:50', '13:55', '14:00', '14:05', '14:10', '14:15', '14:20', '14:25', '14:30', '14:35', '14:40', '14:45', '14:50', '14:55', '15:00', '15:05', '15:10', '15:15', '15:20', '15:25', '15:30', '15:35', '15:40', '15:45', '15:50', '15:55', '16:00', '16:05', '16:10', '16:15', '16:20', '16:25', '16:30', '16:35', '16:40', '16:45', '16:50', '16:55', '17:00', '17:05', '17:10', '17:15', '17:20', '17:25', '17:30', '17:35', '17:40', '17:45', '17:50', '17:55', '18:00', '18:05', '18:10', '18:15', '18:20', '18:25', '18:30', '18:35', '18:40', '18:45', '18:50', '18:55', '19:00', '19:05', '19:10', '19:15', '19:20', '19:25', '19:30', '19:35', '19:40', '19:45', '19:50', '19:55', '20:00', '20:05', '20:10', '20:15', '20:20', '20:25', '20:30', '20:35', '20:40', '20:45', '20:50', '20:55', '21:00', '21:05', '21:10', '21:15', '21:20', '21:25', '21:30', '21:35', '21:40', '21:45', '21:50', '21:55', '22:00', '22:05', '22:10', '22:15', '22:20', '22:25', '22:30', '22:35', '22:40', '22:45', '22:50', '22:55', '23:00', '23:05', '23:10', '23:15', '23:20', '23:25', '23:30', '23:35', '23:40', '23:45', '23:50', '23:55'];
      var r = $('#hours_slider').slider().on('slide', updateHours).data('slider');
      var r2 = $('#hours_slider2').slider().on('slide', updateHour).data('slider');
      var theme = [{"stylers":[{"saturation":-100},{"gamma":1}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"saturation":50},{"gamma":0},{"hue":"#50a5d1"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#333333"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"weight":0.5},{"color":"#333333"}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"gamma":1},{"saturation":50}]}];
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };
      var base_url = "<?php echo base_url();?>";


      function initMap() {
        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer;

        var juarez = {lat: 31.6910036, lng: -106.4489355};
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: juarez,
          disableDefaultUI: true,
          styles: theme
        });

        // directionsDisplay.setMap(map);

        getTrips();

        // ********** AUTOCOMPLETE **********
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete1 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete1')),
            {types: ['geocode']});

        autocomplete2 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete2')),
            {types: ['geocode']});
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

      function ADR2LATLNG(){
        addressToLatLng('autocomplete1');
        addressToLatLng('autocomplete2');
      }

      function addressToLatLng(id){
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            "address": $('#'+id).val()
        }, function(results) {
          tempcoorvar = results[0].geometry.location.lat() + "," + results[0].geometry.location.lng()
          $('#' + id + '-coordenadas').val(tempcoorvar)
        });
      }

      function getTrips(){
        var trips = [{
                      id: 1,
                      nombre: "Ana",
                      rating: 4.6,
                      espacios: 3,
                      type: "nontemp",
                      fecha: "2017-12-01 15:30",
                      origenDireccion: "Catedral Centro",
                      destinoDireccion: "UACJ IADA",
                      origenCoordenadas: {lat: 31.7387934, lng: -106.4892056},
                      destinoCoordenadas: "31.7420431,-106.4353465"
                     },{
                       id: 2,
                       nombre: "Juan",
                       rating: 4.3,
                       espacios: 2,
                       type: "nontemp",
                       fecha: "2017-12-04 15:30",
                       origenDireccion: "UACJ ICB",
                       destinoDireccion: "Las Misiones",
                       origenCoordenadas: {lat: 31.7464483, lng: -106.4459538},
                       destinoCoordenadas: "31.7464291,-106.513806"
                     },{
                       id: 3,
                       nombre: "Alex",
                       rating: 4.9,
                       espacios: 1,
                       type: "nontemp",
                       fecha: "2017-12-03 15:30",
                       origenDireccion: "Las Torres",
                       destinoDireccion: "UACJ ICSA",
                       origenCoordenadas: {lat: 31.6557222, lng: -106.3930494},
                       destinoCoordenadas: "31.7522306,-106.4503569"
                     }];

        for (var i = 0; i < trips.length; i++) {
           marker = new google.maps.Marker({
              position: trips[i]['origenCoordenadas'],
              map: map,
              icon: base_url + "img/icons/bluemarker.png",
              tripInfo: trips[i]
          });
          markers[i] = marker;
          markers[i].addListener('click', function() {
            // console.log(this.tripInfo.id);
            setMapOnAll(null);
            setMapOnOne(map, this.tripInfo.id);
            placeMarker(map, this.tripInfo.destinoCoordenadas);
            updatePanelInfo(this.tripInfo);
            calculateAndDisplayRoute(directionsService, directionsDisplay, this.tripInfo.origenCoordenadas, this.tripInfo.destinoCoordenadas);
          });
        }
      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay, origenCoords, destinoCoords) {
        directionsService.route({
          origin: origenCoords,
          destination: destinoCoords,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }

      function setMapOnAll(mapornull) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(mapornull);
          if(markers[i]['type'] == 'temp'){
            markers.pop(i);
          }
        }
      }

      function setMapOnOne(mapornull, markerid){
        for (var i = 0; i < markers.length; i++) {
          if(markers[i].tripInfo.id == markerid){
            markers[i].setMap(mapornull);
          }
        }
      }

      function placeMarker(map, coordenadas){
        tempDestinationCoords = (coordenadas).split(",")
        marker = new google.maps.Marker({
           position: {lat: parseFloat(tempDestinationCoords[0]), lng: parseFloat(tempDestinationCoords[1])},
           map: map,
           icon: base_url + "img/icons/redmarker.png",
           type: "temp"
       });
       markers.push(marker);
      }

      function updatePanelInfo(tripInfo){
        directionsDisplay.setMap(map);
        $('#panel-conductorNombre').html("<strong>Nombre: </strong>" + tripInfo.nombre);
        $('#panel-conductorRating').html("<strong>Calificacion: </strong>" + tripInfo.rating + "/5");
        $('#panel-origenDireccion').html("<strong>Origen: </strong>" + tripInfo.origenDireccion);
        $('#panel-destinoDireccion').html("<strong>Destino: </strong>" + tripInfo.destinoDireccion);
        $('#panel-fecha').html("<strong>Fecha: </strong>" + tripInfo.fecha);
        $('#panel-pasajeros').html('<strong>Espacios: </strong></strong><i class="fa fa-users" aria-hidden="true"></i> x' + tripInfo.espacios);
        $('#tripPanel').fadeIn();
      }

      function mapaReload(){
        setMapOnAll(null);
        directionsDisplay.setMap(null);
        setMapOnAll(map);
        $('#tripPanel').fadeOut();
      }

      function updateHours(){
        hora1 = r.getValue()[0];
        hora2 = r.getValue()[1];
        $('#hora1').val(hours[hora1]);
        $('#hora2').val(hours[hora2]);
      }

      function updateHour(){
        hora3 = r2.getValue();
        $('#hora3').val(hours[hora3]);
      }

      function filtrarViajes(dia, hora1, hora2, destino){
        $.ajax({
          type: "POST",
          url: base_url + "aventones/filterRequest",
          data: {
            day: dia,
            hour1: hora1,
            hour2: hora2,
            to: destino
          },
          success: function(data){
            // CLEAR OLD markers
            setMapOnAll(null);
            markers = [];
            trips = [];
            for (var i = 0; i < data.length; i++) {
              trips[i] = {
                id: data[i].vId,
                nombre: data[i].uNombre,
                rating: 4.6,
                espacios: data[i].vEspacios,
                type: "nontemp",
                fecha: data[i].vFecha,
                origenDireccion: data[i].vOrigen,
                destinoDireccion: data[i].vDestino,
                origenCoordenadas: data[i].origenCoordenadas,
                destinoCoordenadas: data[i].destinoCoordenadas
              }
            }


            for (var i = 0; i < trips.length; i++) {
              tempOriginCoords = (trips[i].origenCoordenadas).split(",")
               marker = new google.maps.Marker({
                  position: {lat: parseFloat(tempOriginCoords[0]), lng: parseFloat(tempOriginCoords[1])},
                  map: map,
                  icon: base_url + "img/icons/bluemarker.png",
                  tripInfo: trips[i]
              });
              markers[i] = marker;
              markers[i].addListener('click', function() {
                // console.log(this.tripInfo.id);
                setMapOnAll(null);
                setMapOnOne(map, this.tripInfo.id);
                placeMarker(map, this.tripInfo.destinoCoordenadas);
                updatePanelInfo(this.tripInfo);
                calculateAndDisplayRoute(directionsService, directionsDisplay, this.tripInfo.origenCoordenadas, this.tripInfo.destinoCoordenadas);
              });
            }
          },
          dataType: 'JSON'
        });
      }


      $(document).ready(function(){
        // ************** MAPA **************
        $('#tripPanel-close').click(function(){
          mapaReload();
        });


        // FILTROS
        $('#hora1').val(hours[72]);
        $('#hora2').val(hours[252]);
        $('#hora3').val(hours[144]);

        $('#filtro-boton').click(function(){
          filtro_dia = $('#filtro-dia').val();
          filtro_hora1 = $('#hora1').val();
          filtro_hora2 = $('#hora2').val();
          filtro_destino = $('#filtro-destino').val();
          filtrarViajes(filtro_dia, filtro_hora1, filtro_hora2, filtro_destino);
        });


        // ************* LISTA  *************

        // *********** AVENTONES ************

      });

    </script>

    <!-- Google Maps API -->
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzAED8CJdAzjSv6Q5evZDYNr7qLX83fzs&libraries=places&callback=initMap">
    </script>

</body>

</html>
